TOOLCHAIN_TARBALL_gcclinaro_6_3_aarch64 := gcc-linaro-6.3.1-2017.05-x86_64_aarch64-linux-gnu.tar.xz
TOOLCHAIN_TARBALL_HASH_gcclinaro_6_3_aarch64 := d01a380486f1c4cd8c1eadafa8c6a25b335f47cf
TOOLCHAIN_MULTIARCH_gcclinaro_6_3_aarch64 := aarch64-linux-gnu
TOOLCHAIN_COMPATIBLE_DISTROS_gcclinaro_6_3_aarch64 := debian9
TOOLCHAIN_CROSS_COMPILE_gcclinaro_6_3_aarch64 := aarch64-linux-gnu-

AVAILABLE_TOOLCHAINS += gcclinaro_6_3_aarch64
