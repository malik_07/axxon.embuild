ARCH_arm64 = arm64
ARCH_FLAGS_arm64 = -march=armv8-a 

SUPPORTED_DISTROS_arm64 = debian9
TOOLCHAIN_arm64_debian9 = gcclinaro_6_3_aarch64

AVAILABLE_CONFIGS += arm64
AVAILABLE_ARCHITECTURES += arm64
