vcs.hg.fetch = hg clone --noupdate "$(1)" "$(2)"
vcs.hg.pull = hg pull "$(1)"
vcs.hg.archive = hg -R "$(1)" archive -t files -r $(2) "$(3)"

vcs.git.fetch = git clone --no-checkout "$(1)" "$(2)"
vcs.git.pull = git fetch "$(1)"
vcs.git.archive = git --git-dir="$(1)/.git" archive --format=tar $(2) | tar x -C "$(3)"

# register-hg-repository name url destination commit
define register-vcs-repository

REGISTERED_REPOSITORIES += $(1)
REGISTERED_REPOSITORY_URL_$(1) := $(2)
REGISTERED_REPOSITORY_COMMIT_$(1) := $(4)

$(1)-fetch-repository $(REPOCACHE_DIR)/$(1) :
	mkdir -p $(REPOCACHE_DIR)
	if [ ! -d "$(REPOCACHE_DIR)/$(1)" ] ; then \
	    cd "$(REPOCACHE_DIR)" && $(call vcs.$(5).fetch,$(2),$(1)) ; \
	else \
	    cd "$(REPOCACHE_DIR)/$(1)" && $(call vcs.$(5).pull,$(2)) ; \
	fi

$(1)-repository-checkout: $(1)-fetch-repository
	if [ ! -d "$(3)" ] ; then \
	    mkdir -p "$(3)" && \
	    $(call vcs.$(5).archive,$(REPOCACHE_DIR)/$(1),$(4),$(3)) ; \
	else \
		echo "=== NOT UPDATING BUILD DIRECTORY $(3) ==="; \
	fi

endef
register-hg-repository = $(call register-vcs-repository,$(1),$(2),$(3),$(4),hg)
register-git-repository = $(call register-vcs-repository,$(1),$(2),$(3),$(4),git)


TARBALL_ARCHIVE ?= http://fs.itvgroup.cxm/devlibraries/ngp-sdk/src

# register-source-tarball name tarball-name sha1hash
define register-source-tarball

REGISTERED_TARBALLS += $(1)
REGISTERED_TARBALL_FILE_$(1) := $(2)
REGISTERED_TARBALL_HASH_$(1) := $(3)

.PHONY: $(1)-fetch-tarball $(1)-verify-tarball

$(1)-fetch-tarball $(DOWNLOADS_DIR)/$(2) : | $(DOWNLOADS_DIR)
	if [ -e "$(TARBALL_ARCHIVE)/$(2)" ] ; then \
		ln -sf "$(TARBALL_ARCHIVE)/$(2)" "$(DOWNLOADS_DIR)/$(2)" ; \
	else \
		wget -nv "$(TARBALL_ARCHIVE)/$(2)" -O "$(DOWNLOADS_DIR)/$(2)" || { rm -f "$(DOWNLOADS_DIR)/$(2)" ; exit 1; } ; \
	fi

$(1)-verify-tarball: $(DOWNLOADS_DIR)/$(2)
	hash=`expr substr "$$$$(sha1sum $(DOWNLOADS_DIR)/$(2))" 1 40`; \
	[ "$$$$hash" = "$(3)" ] || \
		{ echo "$(2) hash verification failed (expected: $(3); actual: $$$$hash)"; exit 1 ; }

endef

