ARCH_armhf = armhf
ARCH_FLAGS_armhf = -march=armv7-a -mfloat-abi=hard -mfpu=vfpv3-d16

SUPPORTED_DISTROS_armhf = debian9
TOOLCHAIN_armhf_debian9 = gcclinaro_6_3

AVAILABLE_CONFIGS += armhf
AVAILABLE_ARCHITECTURES += armhf
