#!/usr/bin/make -f
SHELL := /bin/bash

.PHONY: help
help:

#
# Return a path to a makefile $(get_current_makefile) is called from, like
# __FILE__ in C++
#
get_current_makefile = $(abspath $(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST)) )

MAKEFLAGS += --no-builtin-rules --no-builtin-variables

ROOT_DIR := $(CURDIR)
REVISION := $(shell git describe --match=NeVeRmAtCh --always --abbrev=12 --dirty=+)

# At the moment, GNU/Linux and Cygwin is all we care about, for other options
# see https://en.wikipedia.org/wiki/Uname
ifeq ($(shell uname -o),GNU/Linux)
  BUILD_OS := LINUX
else
  BUILD_OS := CYGWIN
endif

BUILD_OS_$(BUILD_OS) := $(BUILD_OS)
BUILD_ARCH := $(shell uname -m)

ifndef $(NCPUS)
NCPUS := $(shell getconf _NPROCESSORS_ONLN)
export NCPUS
endif

SYSROOT = /embuild/sysroot

include config/*.mk
include toolchain/*.mk

CONFIG ?= $(firstword $(AVAILABLE_CONFIGS))
TARGET_OS ?= $(firstword $(SUPPORTED_DISTROS_$(CONFIG)))

TOOLCHAIN = $(TOOLCHAIN_$(CONFIG)_$(TARGET_OS))

.PHONY: __check-parameters-sanity
__check-parameters-sanity:
ifeq ($(filter $(CONFIG),$(AVAILABLE_CONFIGS)),)
	@echo "Target $(CONFIG) is not supported"
	exit 1
else ifeq ($(filter $(TARGET_OS),$(SUPPORTED_DISTROS_$(CONFIG))),)
	@echo "Target $(CONFIG) is not compatible with $(TARGET_OS). SUPPORTED_DISTROS_$(CONFIG)=$(SUPPORTED_DISTROS_$(CONFIG))"
	exit 1
else ifeq ($(TOOLCHAIN),)
	@echo "Target $(CONFIG) is not compatible with $(TARGET_OS)"
	exit 1
else ifeq ($(filter $(TARGET_OS),$(TOOLCHAIN_COMPATIBLE_DISTROS_$(TOOLCHAIN))),)
	@echo "Toolchain $(TOOLCHAIN) is not compatible with $(TARGET_OS). TOOLCHAIN_COMPATIBLE_DISTROS_$(TOOLCHAIN)=$(TOOLCHAIN_COMPATIBLE_DISTROS_$(TOOLCHAIN))"
	exit 1
endif

.PHONY: __print-header
__print-header:
	@echo "BUILD_OS: $(BUILD_OS), NCPUS: $(NCPUS), CONFIG: $(CONFIG), TARGET_OS: $(TARGET_OS), TOOLCHAIN: $(TOOLCHAIN)"

.PHONY: __before
__before: __check-parameters-sanity __print-header

map-distro = $(foreach distro,$(SUPPORTED_DISTROS_$(1)),$(call $(2),$(1),$(distro)))
platform-matrix = $(foreach config,$(AVAILABLE_CONFIGS),$(call map-distro,$(config),$(1)))
echo-distro = $(2)
echo-config = $(1)

# A literal space.
space :=
space +=

# Joins elements of the list in arg 2 with the given separator.
#   1. Element separator.
#   2. The list.
join-with = $(subst $(space),$1,$(strip $2))

include makesys/setup_compilers.mk

TARGET_ARCH = $(ARCH_$(CONFIG))
MULTIARCH = $(TOOLCHAIN_MULTIARCH_$(TOOLCHAIN))
CROSS_COMPILE = $(TOOLCHAIN_CROSS_COMPILE_$(TOOLCHAIN))
ASFLAGS = $(ASFLAGS_$(CONFIG))
CFLAGS = $(CFLAGS_$(CONFIG))
CXXFLAGS = $(CXXFLAGS_$(CONFIG))
LDFLAGS = $(LDFLAGS_$(CONFIG))
CC = $(TOOLCHAIN_CC_$(TOOLCHAIN))
ifeq ($(CC),)
CC = gcc
endif
CXX = $(TOOLCHAIN_CXX_$(TOOLCHAIN))
ifeq ($(CXX),)
CXX = g++
endif
TOOLCHAIN_TARBALL = $(TOOLCHAIN_TARBALL_$(TOOLCHAIN))
TOOLCHAIN_PACKAGES = $(TOOLCHAIN_PACKAGES_$(TOOLCHAIN)_$(TARGET_OS))

BUILD_ID := $(CONFIG).$(TARGET_OS)
BUILD_ROOT := $(ROOT_DIR)/build
BUILD_DIR := $(BUILD_ROOT)/$(BUILD_ID)
REPOCACHE_DIR := $(ROOT_DIR)/repos
DOWNLOADS_DIR ?= $(BUILD_DIR)

EMBUILD_REVISION := $(shell git -C "$(ROOT_DIR)" rev-parse HEAD 2>/dev/null || awk '"node:" == $$1 {print $$2}' "$(ROOT_DIR)/.git_archival.txt" 2>/dev/null || echo "unknown")

$(sort $(BUILD_ROOT) $(BUILD_DIR) $(REPOCACHE_DIR) $(DOWNLOADS_DIR)):
	mkdir -p "$@"

help:
	@echo "Supported architectures: $(AVAILABLE_ARCHITECTURES)"
	@echo "Supported platforms: $(AVAILABLE_CONFIGS)"
	@echo "Supported toolchains: $(AVAILABLE_TOOLCHAINS)"
	@echo "Supported OS distributions: $(sort $(call platform-matrix,echo-distro))"
	@echo
	@echo Registered repositories:
	@$(foreach repo,$(REGISTERED_REPOSITORIES), \
		echo "  * $(repo): $(REGISTERED_REPOSITORY_URL_$(repo)) commit $(REGISTERED_REPOSITORY_COMMIT_$(repo))" ; )
	@echo
	@echo Tarball archive: $(TARBALL_ARCHIVE)
	@echo Registered tarballs:
	@$(foreach tarball,$(REGISTERED_TARBALLS), \
		echo "  * $(tarball): $(REGISTERED_TARBALL_FILE_$(tarball)) sha1 $(REGISTERED_TARBALL_HASH_$(tarball))" ; )
	@echo
	@echo "Syntax: make $(call join-with,|,$(AVAILABLE_TARGETS)) [parameter=value ...]"
	@echo "Supported parameters:"
	@echo "  * CONFIG=<config>\t- target platform"
	@echo "  * TARGET_OS=<os-release>\t- target OS release"
	@echo
	@echo "Example: make CONFIG=$(firstword $(AVAILABLE_CONFIGS)) toolchain"
	@echo "Example: make $(lastword $(AVAILABLE_CONFIGS))-$(TARGET_OS)-docker-image DOCKERFILE=/project/Dockerfile TAG=project-embuild-$(lastword $(AVAILABLE_CONFIGS)):$(TARGET_OS)"

.PHONY: clean purge

clean:
	rm -rf "$(BUILD_ROOT)"

purge:
	hg --config extensions.purge= purge --all

include makesys/sources.mk
include makesys/toolchain.mk
include makesys/docker.mk
include makesys/shell.mk
