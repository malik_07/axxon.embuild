#!/bin/sh

set -e

echo "I: Replacing absolute symbolic links to libraries/includes with relative"
find ${EMBUILD_SYSROOT}/ -type d \( -name lib -o -name include \) 2>/dev/null | while read DIR
   do
      find $DIR -type l | while read LINK
         do DEST=`readlink $LINK`
         if ! test ${DEST%%/*} # link is absolute
         then
            REL_DEST="`dirname ${LINK#${EMBUILD_SYSROOT}/} | sed 's@[^/]\{1,\}@..@g'`${DEST#${EMBUILD_SYSROOT}}"
            rm -f "$LINK" && ln -fs "$REL_DEST" "$LINK"
            echo "Symbolic link ${LINK#${EMBUILD_SYSROOT}} was changed from $DEST to $REL_DEST"
         fi
      done
   done

shrink_absolute_paths_in_ld_script ()
{
    sed '/^[[:space:]]*GROUP[[:space:]]*(/{ s@[[:space:]]\([/][^/[:space:]]\+\)*[/]@ @g }' -i "$@"
}

echo "I: Shrinking absolute symbolic links to libraries within GNU ld scripts"
ld_scripts="usr/lib/${EMBUILD_MULTIARCH}/libpthread.so usr/lib/${EMBUILD_MULTIARCH}/libc.so"
for lib in $ld_scripts
do
   if file "${EMBUILD_SYSROOT}/$lib" | grep -q 'ASCII text'
   then
      shrink_absolute_paths_in_ld_script "${EMBUILD_SYSROOT}/$lib"
   fi
done
