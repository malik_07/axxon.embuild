docker.exported-vars = CONFIG TARGET_ARCH MULTIARCH CROSS_COMPILE CC CXX ASFLAGS CFLAGS CXXFLAGS LDFLAGS TARGET_OS DOCKERFILE TAG EMBUILD_REVISION SYSROOT

ifneq ($(TOOLCHAIN_TARBALL),)
docker.exported-vars += TOOLCHAIN_TARBALL
docker.external-toolchain := $(ROOT_DIR)/docker/$(TOOLCHAIN_TARBALL)

.INTERMEDIATE: $(docker.external-toolchain)
$(docker.external-toolchain): toolchain
	ln --logical -f "$(DOWNLOADS_DIR)/$(@F)" "$(ROOT_DIR)/docker/"

else
docker.exported-vars += TOOLCHAIN_PACKAGES

endif

.PHONY: docker-image __docker-image-parameter-is-not-defined
docker-image: __before $(if $(DOCKERFILE),,__docker-image-parameter-is-not-defined) $(if $(TAG),,__docker-image-parameter-is-not-defined) $(docker.external-toolchain)
	$(ROOT_DIR)/docker/prepare-image-with-sysroot.sh $(foreach var,$(docker.exported-vars),$(var)="$($(var))" )

__docker-image-parameter-is-not-defined:
	@echo "USAGE: make docker-image CONFIG=<config> [TARGET_OS=<target-os>] DOCKERFILE=</path/to/input/Dockerfile> TAG=<tag-for-output-image>"
	exit 1

.PHONY: docker-clean-images
docker-clean-images:
	docker rmi $$(docker images -f "dangling=true" -q)

define platform_docker_image_template
.PHONY: $(1)-$(2)-docker-image
$(1)-$(2)-docker-image:
	$$(MAKE) docker-image CONFIG=$(1) TARGET_OS=$(2)
endef

add-platform-docker-image = $(eval $(call platform_docker_image_template,$(1),$(2)))

$(call platform-matrix, add-platform-docker-image)

AVAILABLE_TARGETS += docker-image
