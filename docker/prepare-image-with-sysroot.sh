#!/bin/bash

set -e

CLEANUP="true"
add_cleanup_action ()
{
    CLEANUP="$@
$CLEANUP"
}

cleanup ()
{
    local action
    echo "$CLEANUP" | while read action; do eval $action || true; done
}

on_exit ()
{
    cleanup
}
trap on_exit EXIT INT

TTYARG=
[ ! -t 0 ] || TTYARG=--tty

log ()
{
    echo "$(date)" "$@"
}

header ()
{
    log "*** " "$@" " ***"
}

run_container ()
{
    docker run --rm -i ${TTYARG} "$@"
}

run_cmd_on_auxillary_container ()
{
    docker run --rm -i axxonsoft/axxon-next-client:build "$@"
}

image_hash ()
{
    docker inspect --format='{{.Id}}' $1 2>/dev/null
}

image_parent_hash ()
{
    docker inspect --format='{{.Parent}}' $1 2>/dev/null
}

calc_file_hash ()
{
    sha256sum "$1" | cut -f1 -d ' '
}

write_file_hash ()
{
    calc_file_hash "$1" > "${1}.sha256sum"
}

read_file_hash ()
{
    cat "${1}.sha256sum"
}

is_file_hash_up_to_date ()
{
    local old_hash new_hash
    old_hash="$(read_file_hash "$1")" && new_hash="$(calc_file_hash "$1")" && test "${new_hash}" = "${old_hash}"
}

verbose ()
{
    log "==> " "$@"
    "$@"
}

MANDATORY_VARS="CONFIG TARGET_ARCH TARGET_OS TAG DOCKERFILE MULTIARCH CROSS_COMPILE CC CXX ASFLAGS CFLAGS CXXFLAGS LDFLAGS EMBUILD_REVISION SYSROOT"

usage ()
{
    echo "USAGE: $0 [-h|--help] {parameter=value}"
    echo "Supported parameters:"
    echo "\t${MANDATORY_VARS} TOOLCHAIN_TARBALL|TOOLCHAIN_PACKAGES"
}

DOCKER_DIR="$(dirname "$0")"
DOCKER_DIR="$(cd "${DOCKER_DIR:-.}" && pwd)"

for arg; do
    case "$arg" in
        *=*)
            eval "${arg%%=*}='${arg#*=}'" ;;
        --help|-h)
            usage
            exit 1 ;;
        -*)
            echo "ERROR: unknown option $arg"
            usage
            exit 1 ;;
        *)
            echo "ERROR: unknown argument $arg"
            usage
            exit 1 ;;
    esac
done

for var in ${MANDATORY_VARS}; do
    if ! eval : "\${${var}?}" 2>/dev/null ; then
        echo "ERROR: $var was not specified"
        exit 1
    fi
done

if test -z "${TOOLCHAIN_TARBALL}${TOOLCHAIN_PACKAGES}"; then
    echo "ERROR: either TOOLCHAIN_TARBALL or TOOLCHAIN_PACKAGES must be specified"
    exit 1
fi

case "${TARGET_OS}" in
    debian9)
        EMBUILD_FROM=debian:stretch ;;
    *)
        echo "ERROR: $0 does not support target distribution ${TARGET_OS}" ;;
esac

header "Building auxillary image embuild-debootstrap-ready-${TARGET_ARCH}:${TARGET_OS}"
PULL_ARG=--pull
${DOCKER_BUILD_WITH_PULL:-true} || PULL_ARG=
verbose docker build ${PULL_ARG} -f "${DOCKER_DIR}/Dockerfile.debootstrap_ready" \
    --build-arg EMBUILD_FROM=${EMBUILD_FROM} \
    --build-arg EMBUILD_ARCH=${TARGET_ARCH} \
    --build-arg EMBUILD_SYSROOT=${SYSROOT} \
    --tag=embuild-debootstrap-ready-${TARGET_ARCH}:${TARGET_OS} \
    "${DOCKER_DIR}"
header "Building sysroot image embuild-sysroot-${TARGET_ARCH}:${TARGET_OS}"
if [ "$(image_hash embuild-debootstrap-ready-${TARGET_ARCH}:${TARGET_OS} 2>/dev/null)" != "$(image_parent_hash embuild-sysroot-${TARGET_ARCH}:${TARGET_OS})" ]; then
    add_cleanup_action "verbose docker rm embuild-debootstrap-ready-${TARGET_ARCH}-for-${TARGET_OS} 2>/dev/null"
    verbose docker run -i ${TTYARG} --privileged --name embuild-debootstrap-ready-${TARGET_ARCH}-for-${TARGET_OS} embuild-debootstrap-ready-${TARGET_ARCH}:${TARGET_OS} /embuild/create_foreign_sysroot.sh
    verbose docker commit embuild-debootstrap-ready-${TARGET_ARCH}-for-${TARGET_OS} embuild-sysroot-${TARGET_ARCH}:${TARGET_OS}
    log "Image embuild-sysroot-${TARGET_ARCH}:${TARGET_OS} is ready"
else
    log "Sysroot image embuild-sysroot-${TARGET_ARCH}:${TARGET_OS} is up-to-date"
fi

if [ -n "${TOOLCHAIN_TARBALL}" ]; then
    header "Adding toolchain ${TOOLCHAIN_TARBALL}"
    docker build -f "${DOCKER_DIR}/Dockerfile.toolchain_tarball" \
        --build-arg EMBUILD_FROM=embuild-sysroot-${TARGET_ARCH}:${TARGET_OS} \
        --build-arg TOOLCHAIN_TARBALL="${TOOLCHAIN_TARBALL}" \
        --build-arg EMBUILD_MULTIARCH="${MULTIARCH}" \
        --build-arg EMBUILD_CROSS_COMPILE="${CROSS_COMPILE}" \
        --build-arg EMBUILD_CC="${CC}" \
        --build-arg EMBUILD_CXX="${CXX}" \
        --tag=embuild-sysroot-toolchain-${CONFIG}:${TARGET_OS} \
        "${DOCKER_DIR}"
else
    header "Installing toolchain from packages ${TOOLCHAIN_PACKAGES}"
    docker build -f "${DOCKER_DIR}/Dockerfile.toolchain_packages" \
        --build-arg EMBUILD_FROM=embuild-sysroot-${TARGET_ARCH}:${TARGET_OS} \
        --build-arg TOOLCHAIN_PACKAGES="${TOOLCHAIN_PACKAGES}" \
        --build-arg EMBUILD_MULTIARCH="${MULTIARCH}" \
        --build-arg EMBUILD_CROSS_COMPILE="${CROSS_COMPILE}" \
        --build-arg EMBUILD_CC="${CC}" \
        --build-arg EMBUILD_CXX="${CXX}" \
        --tag=embuild-sysroot-toolchain-${CONFIG}:${TARGET_OS} \
        "${DOCKER_DIR}"
fi
log "Container embuild-sysroot-toolchain-${CONFIG}:${TARGET_OS} is ready"

header "Applying target Dockerfile (${DOCKERFILE})"
docker build -f "${DOCKERFILE}" \
        --build-arg EMBUILD_FROM=embuild-sysroot-toolchain-${CONFIG}:${TARGET_OS} \
        --tag=embuild-${TAG} \
        "$(dirname ${DOCKERFILE})"

header "Finalizing image ${TAG}"
docker build -f "${DOCKER_DIR}/Dockerfile.finalize" \
        --build-arg EMBUILD_FROM=embuild-${TAG} \
        --build-arg EMBUILD_CONFIG=${CONFIG} \
        --build-arg EMBUILD_OS=${TARGET_OS} \
        --build-arg EMBUILD_ASFLAGS="${ASFLAGS}" \
        --build-arg EMBUILD_CFLAGS="${CFLAGS}" \
        --build-arg EMBUILD_CXXFLAGS="${CXXFLAGS}" \
        --build-arg EMBUILD_LDFLAGS="${LDFLAGS}" \
        --build-arg EMBUILD_REVISION="${EMBUILD_REVISION}" \
        --tag=${TAG} \
        "${DOCKER_DIR}"

log "Container ${TAG} is ready"

