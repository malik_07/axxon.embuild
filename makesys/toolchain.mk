.PHONY: toolchain
toolchain: __before
ifneq ($(TOOLCHAIN_TARBALL_HASH_$(TOOLCHAIN)),)

toolchain: toolchain-$(TOOLCHAIN)-verify-tarball

endif

define platform_toolchain_template
.PHONY: $(1)-$(2)-toolchain
$(1)-$(2)-toolchain:
	$$(MAKE) toolchain CONFIG=$(1) TARGET_OS=$(2)
endef

add-platform-toolchain = $(eval $(call platform_toolchain_template,$(1),$(2)))

$(call platform-matrix, add-platform-toolchain)

define toolchain_template
ifneq ($$(TOOLCHAIN_TARBALL_HASH_$(1)),)

$$(eval $$(call register-source-tarball,toolchain-$(1),$$(TOOLCHAIN_TARBALL_$(1)),$$(TOOLCHAIN_TARBALL_HASH_$(1))))

endif
endef

$(foreach toolchain, $(AVAILABLE_TOOLCHAINS), $(eval $(call toolchain_template,$(toolchain))))

AVAILABLE_TARGETS += toolchain

