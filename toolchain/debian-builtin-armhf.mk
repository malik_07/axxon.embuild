TOOLCHAIN_PACKAGES_debian_builtin_armhf_debian9 := g++-arm-linux-gnueabihf
TOOLCHAIN_MULTIARCH_debian_builtin_armhf := arm-linux-gnueabihf
TOOLCHAIN_COMPATIBLE_DISTROS_debian_builtin_armhf := debian9 debian8 debian7
TOOLCHAIN_CROSS_COMPILE_debian_builtin_armhf := arm-linux-gnueabihf-

AVAILABLE_TOOLCHAINS += debian_builtin_armhf

