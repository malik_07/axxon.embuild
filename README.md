# Purpose

The project provides a generic mechanism that can be easily integrated into
build system of any C/C++ project (of AxxonSoft) for setting up cross-building
environment for embedded targets.

The project is supposed to cover:

* building of development sysroot eligible for the subsequent tuning according
  to a wrapping project needs with all tools necessary for such tuning;
* establishing of a single source of truth for platform-specific compilation
  flags;
* building of production sysroot (optionally);
* building of kernel and u-boot (or any other device specific bootloader);
* building device-specific OpenGL-related modules and installing it onto
  dev/production sysroot;
* building other platform SDK and installing in onto dev/production sysroot;
* publishing of built artifacts over NFS (sysroots) and TFTP (kernel/uboot).

# Relationships to other projects

It is the 3rd try to achieve the goals specified above. The previous were:

1. [dm8168](https://bitbucket.org/a-nikolaevsky/dm8168)
2. [embuild](https://bitbucket.org/a-nikolaevsky/embuild)

The key differences from the previous projects are:

* using Docker instead of multistrap + fakeroot + fakechroot for making
  cross-building environment;
* previously projects were built from the embuild shell, support for some of
  them was directly included into the embuild build system (e.g. NGP), now every
  project that requires cross-building is supposed to plug embuild's API into its
  own build system.

# API

## Building Docker container for cross-building
**TO DO**

## Environment variables inside Docker container
* *EMBUILD_ARCH* - Debian's notation of the target architecture
* *EMBUILD_MULTIARCH* - multiarch alias for target architecture
* *EMBUILD_CONFIG* - concrete H/W platform alias
* *EMBUILD_OS* - target OS distribution
* *EMBUILD_SYSROOT* - path to sysroot directory
* *EMBUILD_CROSS_COMPILE* - cross-compiler prefix
* *EMBUILD_CC* - C cross-compiler basename (gcc, clang)
* *EMBUILD_CXX* - C++ cross-compiler basename (g++, clang++)
* *EMBUILD_CFLAGS* - platform CFLAGS
* *EMBUILD_CXXFLAGS* - platform CXXFLAGS
* *EMBUILD_LDFLAGS* - platform LDFLAGS
* *EMBUILD_PKG_CONFIG* - sysroot-aware pkg-config
* *EMBUILD_CMAKE_TOOLCHAIN_FILE* - pre-created CMake toolchain file, can be passed directly to CMake or included into project-specific toolchain file

## Publishing over NFS
**TO DO**

## Publishing over TFTP
**TO DO**

# How-to
There are some useful notes and code snippets in the 'howto' directory :)

# Requirements
* Package qemu-user-static must be installed on the host system to support running binaries built for target architectures
