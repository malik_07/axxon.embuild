ARCH_hi3536 = armhf
ARCH_FLAGS_hi3536 = -march=armv7-a -mtune=cortex-a17 -mfloat-abi=hard -mfpu=neon-vfpv4 -mno-unaligned-access -fno-aggressive-loop-optimizations

SUPPORTED_DISTROS_hi3536 = debian9
TOOLCHAIN_hi3536_debian9 = $(TOOLCHAIN_armhf_debian9)

AVAILABLE_CONFIGS += hi3536

