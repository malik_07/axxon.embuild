#!/bin/sh

set -e

cat /etc/apt/sources.list /etc/apt/sources.list.d/*.list 2>/dev/null | \
    awk '$1 ~ /^#/{next}$1 == "deb"{print $2 " " $3; nextfile}' | {
        read apt_mirror suite
        echo "Preparing building environment [architecture: $arch; APT mirror: $apt_mirror; suite: $suite]"
        debootstrap --foreign --arch ${EMBUILD_ARCH} $suite ${EMBUILD_SYSROOT} $apt_mirror
    }
case "${EMBUILD_ARCH}" in
    armhf)
        QEMU_PLATFORM=arm ;;
    arm64)
        QEMU_PLATFORM=aarch64 ;;
    *)
        echo "ERROR: $0 does not support architecture ${EMBUILD_ARCH}"
        exit 1 ;;
esac
cp -af /usr/bin/qemu-${QEMU_PLATFORM}-static "${EMBUILD_SYSROOT}/usr/bin/"
chroot "${EMBUILD_SYSROOT}/" /debootstrap/debootstrap --second-stage
tar c /etc/apt/sources.list /etc/apt/sources.list.d/*.list 2>/dev/null | tar x -C  ${EMBUILD_SYSROOT}
echo "Upgrading packages"
chroot ${EMBUILD_SYSROOT} sh -c 'apt-get update && apt-get upgrade -y'
echo "Cleaning up"
chroot ${EMBUILD_SYSROOT} sh -c 'apt-get autoclean && apt-get clean && apt-get autoremove && rm -rf /var/lib/apt/lists/*'
echo "Done"

