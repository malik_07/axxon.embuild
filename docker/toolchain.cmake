SET(CMAKE_SYSTEM_NAME Linux)
SET(CMAKE_SYSTEM_PROCESSOR "$ENV{EMBUILD_ARCH}")

# specify the compiler
SET(CMAKE_C_COMPILER   "$ENV{EMBUILD_CROSS_COMPILE}$ENV{EMBUILD_CC}")
SET(CMAKE_CXX_COMPILER "$ENV{EMBUILD_CROSS_COMPILE}$ENV{EMBUILD_CXX}")

# according to http://code.i-harness.com/en/q/16e228b setting CMAKE_<LANG>_FLAGS_INIT
# from a toolchain file is useless since CMake own scripts overrides them later. Thus,
# in order not to interfere with project's own flags we're passing initials through
# common environment
SET(ENV{CFLAGS} "$ENV{EMBUILD_CFLAGS}")
SET(ENV{CXXFLAGS} "$ENV{EMBUILD_CXXFLAGS}")
SET(ENV{LDFLAGS} "$ENV{EMBUILD_LDFLAGS}")

# where is the target environment 
SET(SYSROOT "$ENV{EMBUILD_SYSROOT}")
SET(CMAKE_FIND_ROOT_PATH ${SYSROOT} ${CMAKE_MODULE_PATH})

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
#SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
#SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
SET(CMAKE_CROSSCOMPILING True)
SET(CMAKE_C_COMPILER_TARGET $ENV{EMBUILD_MULTIARCH})
SET(CMAKE_CXX_COMPILER_TARGET $ENV{EMBUILD_MULTIARCH})
