.PHONY: shell
shell:
	@env \
	    'EMBUILD_CONFIG=$(CONFIG)' \
	    'EMBUILD_ARCH=$(TARGET_ARCH)' \
	    'EMBUILD_OS=$(TARGET_OS)' \
	    'EMBUILD_MULTIARCH=$(MULTIARCH)' \
	    'EMBUILD_SYSROOT=$(SYSROOT)' \
	    'EMBUILD_CROSS_COMPILE=$(CROSS_COMPILE)' \
	    'EMBUILD_ASFLAGS=$(ASFLAGS)' \
	    'EMBUILD_CFLAGS=$(CFLAGS)' \
	    'EMBUILD_CXXFLAGS=$(CXXFLAGS)' \
	    'EMBUILD_LDFLAGS=$(LDFLAGS)' \
	    'EMBUILD_PKG_CONFIG=$(ROOT_DIR)/docker/cross-pkg-config' \
	    'EMBUILD_CMAKE_TOOLCHAIN_FILE=$(ROOT_DIR)/docker/toolchain.cmake' \
	    'EMBUILD_REVISION=$(EMBUILD_REVISION)' \
	    bash --rcfile <(cat ~/.bashrc ; echo 'PS1="\[\033[01;32m\][embuild C=$${EMBUILD_CONFIG} A=$${EMBUILD_ARCH}]:\[\033[01;34m\]\w\[\033[00m\]\$$ "')

define config_api_template
.PHONY: $(1)-$(2)-shell
$(1)-$(2)-shell:
	@$$(MAKE) shell CONFIG=$(1) TARGET_OS=$(2) --no-print-directory

endef

$(eval $(call platform-matrix,config_api_template))

AVAILABLE_TARGETS += shell
