TOOLCHAIN_TARBALL_gcclinaro_6_3 := gcc-linaro-6.3.1-2017.05-x86_64_arm-linux-gnueabihf.tar.xz
TOOLCHAIN_TARBALL_HASH_gcclinaro_6_3 := aa19319732518a330e9d9275c6c4721db6d8c7c3
TOOLCHAIN_MULTIARCH_gcclinaro_6_3 := arm-linux-gnueabihf
TOOLCHAIN_COMPATIBLE_DISTROS_gcclinaro_6_3 := debian9
TOOLCHAIN_CROSS_COMPILE_gcclinaro_6_3 := arm-linux-gnueabihf-

AVAILABLE_TOOLCHAINS += gcclinaro_6_3
